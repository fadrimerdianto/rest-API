<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();
require_once 'notORM/NotORM.php';

$local = 'localhost';
$db_name = 'dot';
$db_user = 'root';
$db_pass = '';
$pdo = new PDO("mysql:host=$local;dbname=$db_name", $db_user, $db_pass);
$db = new NotORM($pdo);

\Slim\Slim::registerAutoloader();

function getDB()
{   
    $local = 'localhost';
    $db_name = 'dot';
    $db_user = 'root';
    $db_pass = '';
    $pdo = new PDO("mysql:host=$local;dbname=$db_name", $db_user, $db_pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
}

//ini line ke 33
/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();
$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("X-My-Custom-Header", "X-Another-Custom-Header"),
    "maxAge" => 1728000,
    "allowCredentials" => True,
    "allowMethods" => array("POST, GET"),
    "allowHeaders" => array("X-PINGOTHER")
    );
$app->add(new \CorsSlim\CorsSlim($corsOptions));
/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

// GET route
$app->get('/users',  function ()  use ($app, $db){
    $app->response->headers->set('Content-Type', 'application/json');
    $result = $db->users();
    $message = '';
    if($result)
    {
        $message['status']              = 200;
        foreach ($result as $row)
        {
            $message['result'][] = array(
                'id'        => $row['id'],
                'no_hp'     => $row['no_hp']
            );
        }

    }
    else {
        $message['status']  = 400;
        $message['result']  = 'Get User Failed';
        $message['message'] = 'Failed';
    }
    echo json_encode($message, JSON_PRETTY_PRINT);
});

// register customer
$app->post('/register', function() use ($app, $db){
    $app->response->headers->set('Content-Type', 'application/json');

    $input = $app->request()->post();
    $data = array(
        'id'            => $input['username'],
        'password'      => md5($input['password']),
        'no_hp'         => $input['no_hp'],
        'created_at'    => date('Y-m-d'),
        'updated_at'    => date('Y-m-d'),
        'token'         => uniqid()
    );
    $result = $db->users->insert($data);
    $message = '';
    if($result)
    {
        $message['status']  = 200;
        $message['result']  = 'Register User Success';
        $message['message'] = 'User ' . $input['username'] . ' created';
    }
    else {
        $message['status']  = 400;
        $message['result']  = 'Register User Failed';
        $message['message'] = 'Failed';
    }
    echo json_encode($message, JSON_PRETTY_PRINT);
});

$app->put('/:user/:token', function($user, $token) use ($app, $db){
    $app->response->headers->set('Content-Type', 'application/json');
    $input = $app->request()->put();
    $param = array(
        'token'     => $token,
        'id'        => $user
    );
    $where = $db->users->where($param);
    if($where->fetch())
    {
        $data = array(
            'id'            => $user,
            'no_hp'         => $input['no_hp'],
            'updated_at'    => date('Y-m-d'),
        );
        $result = $where->update($data);
        $message = '';
        if($result)
        {
            $message['status'] = 200;
            $message['result'] = 'Update Success';
            $message['message'] = 'User updated';
        }
        else {
            $message['status'] = 400;
            $message['result'] = 'Update Failed';
            $message['message'] = 'Failed';
        }
    }
    else {
            $message['status'] = 400;
            $message['result'] = 'Auth Failed';
            $message['message'] = 'Failed';
    }
    echo json_encode($message, JSON_PRETTY_PRINT);
});


$app->delete('/:user/:token', function ($user, $token)  use ($app, $db){
    $app->response->headers->set('Content-Type', 'application/json');
    $param = array(
        'token'     => $token,
        'id'        => $user
    );

    $where = $db->users->where($param);
    if($where->fetch())
    {
        $result = $db->users->delete($param);
        //Delete book identified by $id
        if($result)
        {
            $message['status'] = 200;
            $message['result'] = 'Delete Success';
            $message['message'] = 'User deleted';
        }
        else {
            $message['status'] = 400;
            $message['result'] = 'Delete Failed';
            $message['message'] = 'Failed';
        }

    }
    else {
            $message['status'] = 400;
            $message['result'] = 'Auth Failed';
            $message['message'] = 'Failed';
    }
    echo json_encode($message, JSON_PRETTY_PRINT);
});
$app->run();